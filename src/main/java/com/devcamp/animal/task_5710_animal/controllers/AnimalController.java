package com.devcamp.animal.task_5710_animal.controllers;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.animal.task_5710_animal.models.*;

@RestController
public class AnimalController {

    @CrossOrigin
    @GetMapping("listAnimal")
    public ArrayList<Animal> listAnimal() {
        ArrayList<Animal> lstAnimal = new ArrayList<Animal>();
        //Animal animal = new Animal();
        Animal duck = new Duck();
        duck.setAge(2);
        duck.setGender("male");
        ((Duck)duck).setBeakColor("yellow");
        duck.mate();
        duck.isMammal();

        Duck duck2 = new Duck(3, "female", "white");
        duck2.mate();
        duck2.swim();
        duck2.quack();

        Fish fish = new Fish(1, "male", 15, true);
        fish.isMammal();
        fish.mate();
        fish.swim();

        Zebra zebra = new Zebra(5, "male", true);
        zebra.isMammal();
        zebra.mate();
        zebra.run();

        lstAnimal.add(duck);
        lstAnimal.add(duck2);
        lstAnimal.add(fish);
        lstAnimal.add(zebra);
        return lstAnimal;
    }    
}
